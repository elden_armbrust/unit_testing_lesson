<?php

namespace Tests\Unit\Controllers;

use TestCase;
use App\Http\Controllers\ConvertController;
use Mockery as m;
use Laravel\Lumen\Testing\DatabaseMigrations;
use App\Repositories\ConvertRepositoryInterface as ConvertRepository;
use App\Models\Convert;
class ConvertControllerTest extends TestCase
{

    use DatabaseMigrations;
    protected $baseUrl = 'http://localhost';
    protected $testString = 'test';
    /**
     * Tests that the route returns the proper hypermedia links
     * @return void
     */
    public function testRoute()
    {
        $this->withoutMiddleware();
        $expectation = [
            '_links' => [
                [
                    'rel' => 'self',
                    'href' => $this->baseUrl.'/convert',
                    'method' => 'GET',
                ],
                [
                    'rel' => 'convert',
                    'href' => $this->baseUrl.'/convert/{string}',
                    'method' => 'GET',
                ],
            ],
        ];
        $this->json('GET', '/convert')->seeJsonEquals($expectation);
    }

    /**
     * Validates that the model information is delivered by the controller properly,
     * and that proper separation of concerns is observed
     * @return void
     */
    public function testRouteInput()
    {
        $expectation = [
            'input' => $this->testString,
            'sha1' => sha1($this->testString),
            'md5' => md5($this->testString),
            'base64' => base64_encode($this->testString),
        ];

        $this->withoutMiddleware();
        $convertModelMock = m::mock(Convert::class);
        $this->app->instance(Convert::class, $convertModelMock);
        $convertModelMock->shouldReceive('getMd5')->andReturn(md5($this->testString));
        $convertModelMock->shouldReceive('getSha1')->andReturn(sha1($this->testString));
        $convertModelMock->shouldReceive('getBase64')->andReturn(base64_encode($this->testString));
        $convertModelMock->shouldReceive('convert')->andReturn($expectation);


        $this->json('GET', '/convert/'.$this->testString)->seeJsonEquals($expectation);
    }
}

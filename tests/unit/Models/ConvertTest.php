<?php

namespace Tests\Unit\Controllers;

use TestCase;
use App\Models\Convert;
use Mockery as m;
use Laravel\Lumen\Testing\DatabaseMigrations;

class ConvertTest extends TestCase
{
    use DatabaseMigrations;
    private $testStrings = [];
    private $numOfTestStrings = 128;
    private $lenRandomStringSize = 64;

    public function setUp()
    {
        parent::setUp();
        for ($iter = 0; $iter < $this->numOfTestStrings; $iter++) {
            array_push($this->testStrings, str_random($this->lenRandomStringSize));
        }
    }

    public function testCanInstantiate()
    {
        $instanceModel = new Convert();
        $this->assertNotNull($instanceModel);
        $this->assertInstanceOf(Convert::class, $instanceModel);
    }

    public function testCanGetMd5()
    {
        $instanceModel = new Convert();
        foreach ($this->testStrings as $testString) {
            $this->assertSame(md5($testString), $instanceModel->getMd5($testString));
        }
    }
    public function testCanGetSha1()
    {
        $instanceModel = new Convert();
        foreach ($this->testStrings as $testString) {
            $this->assertSame(sha1($testString), $instanceModel->getSha1($testString));
        }
    }
    public function testCanGetBase64()
    {
        $instanceModel = new Convert();
        foreach ($this->testStrings as $testString) {
            $this->assertSame(base64_encode($testString), $instanceModel->getBase64($testString));
        }
    }

    public function testCanGetConvert()
    {

        foreach ($this->testStrings as $testString) {
            $instanceModel = m::mock(Convert::class."[getMd5,getSha1,getBase64]");
            $instanceModel->shouldReceive('getMd5')->andReturn(md5($testString));
            $instanceModel->shouldReceive('getSha1')->andReturn(sha1($testString));
            $instanceModel->shouldReceive('getBase64')->andReturn(base64_encode($testString));
            $expectation = [
                'input' => $testString,
                'sha1' => sha1($testString),
                'md5' => md5($testString),
                'base64' => base64_encode($testString),
            ];
            $this->assertSame($expectation, $instanceModel->convert($testString));
        }
    }
}

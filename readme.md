# Unit Testing Lesson

```
   +--------------+
   |.------------.|
   ||            ||
   ||   Lunch    ||
   ||     n      ||
   ||   Learn    ||
   |+------------+|
   +-..--------..-+
   .--------------.
  / /============\ \
 / /==============\ \
/____________________\
\____________________/
```



* [Helpful Links](https://bitbucket.org/elden_armbrust/unit_testing_lesson/src/HEAD/links.md?at=master&fileviewer=file-view-default) - Helpful links to get you going

## Itinerary

### 00:00 - 03:00 Introduction
I am a nerd.

### 03:00 - 06:00 What is unit testing?
Unit testing is a software development process in which the smallest testable parts of an application, called units, are individually and independently scrutinized for proper operation. Unit testing is often automated but it can also be done manually.

### 06:00 - 10:00 Why should you unit test?
* Tests Reduce Bugs
* Tests Are Good Documentation
* Tests Reduce the Cost of Change
* Tests Improve Design
* Testing Makes Development Faster
* Tests Reduce Fear

### 10:00 - 13:00 What should you unit test?
* Business logic
* External requirements
* Data transformations
* Any unit of logic with a high enough cyclomatic complexity
* etc

### 13:00 - 15:00 When should you unit test?
Unit tests should be written in tandem with new code, ideally.  If working on legacy/untested code, then tests should be added when technically possible whenever a unit of code is touched.

### 15:00 - 18:00 What are the tools?
* PHPUnit
* QUnit
* Mocha
* Jasmine
* Mockery

### 18:00 - 23:00 Q&A
Questions and answers on the fundamentals and base understanding.

### 23:00 - 40:00 How do you unit test?
Demonstration of creating a unit test to cover an existing piece of code.

### 40:00 - 45:00 Final Q&A + Info Share
Distributions of links and final questions
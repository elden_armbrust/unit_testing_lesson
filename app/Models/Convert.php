<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Models\ConvertRepositoryInterface;

class Convert implements ConvertRepositoryInterface
{


    public function getMd5($input)
    {

        return md5($input);
    }

    public function getSha1($input)
    {
        return sha1($input);
    }

    public function getBase64($input)
    {
        return base64_encode($input);
    }

    public function convert($input)
    {
        return [
            'input' => $input,
            'sha1' => $this->getSha1($input),
            'md5' => $this->getMd5($input),
            'base64' => $this->getBase64($input),
        ];
    }
}

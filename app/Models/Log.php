<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Repositories\Models\LogRepositoryInterface;

class Log extends Model implements LogRepositoryInterface
{

    use SoftDeletes;
    protected $fillable = [
        'ip',
        'input'
    ];

    protected $dates = ['deleted_at'];

    public static $rules = [
        // Validation rules
    ];
    // Relationships
}

<?php

namespace App\Providers\Models;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Models\LogRepositoryInterface;
use App\Models\Log;

class LogServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(LogRepositoryInterface::class, Log::class);
    }
}

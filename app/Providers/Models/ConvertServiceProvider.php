<?php

namespace App\Providers\Models;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Models\ConvertRepositoryInterface;
use App\Models\Convert;

class ConvertServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ConvertRepositoryInterface::class, Convert::class);
    }
}

<?php

namespace App\Repositories\Models;

interface ConvertRepositoryInterface
{
    public function getMd5($input);
    public function getSha1($input);
    public function getBase64($input);
}

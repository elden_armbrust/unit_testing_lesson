<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Models\ConvertRepositoryInterface as ConvertRepository;
use App\Repositories\Models\LogRepositoryInterface as LogRepository;

// use Illuminate\Support\Facades\Route as Router;

class ConvertController extends Controller
{
    private $request;
    private $convert;
    private $log;
    /**
     * Create a new controller instance.
     */
    public function __construct(Request $request, ConvertRepository $convert, LogRepository $log)
    {
        $this->request = $request;
        $this->convert = $convert;
        $this->log = $log;
    }

    public function convert($input)
    {
        $this->log->ip = $this->request->ip();
        $this->log->input = $input;
        $this->log->save();
        return $this->convert->convert($input);
    }

    public function index()
    {
        $obj = [
            '_links' => [
                [
                    'rel' => 'self',
                    'href' => $this->request->url(),
                    'method' => 'GET',
                ],
                [
                    'rel' => 'convert',
                    'href' => $this->request->url().'/{string}',
                    'method' => 'GET',
                ],
            ],
        ];

        return $obj;
    }
}
